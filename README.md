# Application de gestion de retrospective

## Quick start

> Vous devez disposer de Docker et Docker SWARM pour suivre ce Quick Start.
> L'application fonctionne bien sans mais l'installation se voit grandement facilitée par la présence de Docker SWARM !

Pour tester la stack à base de Docker Swarm, easy. Créez un Swarm si vous n'en n'avez pas déjà un :

```shell
docker swarm init
```

Puis pour déployer, il faudra compiler les 3 images Docker et enfin déployer le fichier docker-compose via la commande `docker stack ...` de Swarm.








## Quelques soucis majeurs dans la code base

### Problème de "locales" dans l'image backend

Juste après la commande `FROM`, déclarez une timezone dans votre image :

```
ENV TZ=Europe/Paris
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
```

La compilation de l'image ne posera alors plus de souci





### Problème de variables d'environnement backend

Lorsque l'on compile l'image backend, notez que l'on importe tous les fichiers du dossier courant seront copiées dans le conteneur, y compris un fichier `.env`.

Ce fichier .env sera détecté par python au lancement du backend et il surchargera alors les variables d'environnement que vous auriez pu définir dans votre Docker Compose.

Il existe de nombreuses méthodes pour éviter cela :


- décrire dans le fichier .env toutes les bonnes variables (cette méthode ne permet pas de déployer la même image avec différentes variables d'environnement, sans avoir à recompiler... Pas génial) 
- supprimer le fichier .env
- ajouter une fichier .dockerignore qui enregistrera tous les fichiers .env 





### Lien backend BDD

Le docker compose indique que le service BDD expose le port 5432 de la BDD vers un port "ouvert au public".
Il serait une bonne idée de ne pas permettre cela et de profiter du réseau partagé par nos trois conteneurs pour limiter la surface d'exposition vers l'extérieur.

Autre avantage de réaliser cela, le backend pourra accéder à la BDD via son nom de service et sur le port d'écoute de Postgres, à savoir `db:5432`. Cela résoudra ainsi les soucis divers rencontrés durant les journées précédentes sur le TP...




### Une fois le backend configuré avec la BDD, reste l'installation

Un fois que le backend peut accéder à la BDD, il doit réaliser son installation la première fois (créer les tables et utilisateurs).
Pour cela il faut ajuster encore une fois une variable d'environnement dans le docker compose, je vous laisse deviner, hein... Ensuite, vous pourrez redéployer votre stack après avoir changé la valeur de cette variable.




### Lien frontend backend

Le frontend envoie toutes ses requêtes sur le backend en se basant sur une variable d'environnement décrite pour l'occasion.
Notez que cette variable d'environnement doit être fixée durant le build de l'image frontend (pas possible d'accéder aux variables d'environnement en JS compilé car il s'exécute dans le navigateur de l'utilisateur et non sur le serveur)

Il faut donc que vous compiliez l'image frontend en renseignant l'url de votre backend (cette fois vous ne pouvez pas utiliser le DNS de votre service du genre `backend:10001` car le front s'exécute toujours dans le navigateur).
Vous allez donc sans doute renseigner une IP publique que vos aurez identifié ou, plus simplement, un bon vieux `localhost`.


