import { createRouter, createWebHistory, RouteRecordRaw, } from 'vue-router'
import LoginScreen from '../views/LoginScreen.vue'

/**
 * This is a documentation write in english english.
 */
const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: () => import(/* webpackChunkName: "retro" */ '../views/AddFeedback.vue'),
    meta: {
      title: 'Raytro - Gérer ses rétrospectives like a boss',
      metaTags: [
        {
          name: 'description',
          content: 'La meilleure appli pour gérer les feedbacks de vos travailleurs sous-payés.',
        },
        {
          property: 'og:description',
          content: 'La meilleure appli pour gérer les feedbacks de vos travailleurs sous-payés.',
        },
        {
          property: 'og:image',
          content: 'https://raytro.asciiparait.fr/og-visu.png',
        },
        {
          property: 'og:site_name',
          content: 'Raytro',
        },
        {
          property: 'og:type',
          content: 'object',
        },
        {
          property: 'og:title',
          content: 'Raytro | Manage feedbacks like aboss',
        },
        {
          property: 'og:url',
          content: 'https://raytro.asciiparait.fr/',
        },
      ],
    },
  },
  {
    path: '/login',
    name: 'Login',
    component: LoginScreen,
    meta: {
      title: 'Raytro - Page de connexion',
      metaTags: [
        {
          name: 'description',
          content: 'Créer un compte | La meilleure appli pour gérer les feedbacks de vos travailleurs sous-payés.',
        },
        {
          property: 'og:description',
          content: 'Créer un compte | La meilleure appli pour gérer les feedbacks de vos travailleurs sous-payés.',
        },
        {
          property: 'og:image',
          content: 'https://raytro.asciiparait.fr/og-visu.png',
        },
        {
          property: 'og:site_name',
          content: 'Raytro',
        },
        {
          property: 'og:type',
          content: 'object',
        },
        {
          property: 'og:title',
          content: 'Raytro | Manage feedbacks like aboss',
        },
        {
          property: 'og:url',
          content: 'https://raytro.asciiparait.fr/',
        },
      ],
    },
  },
  {
    path: '/feedbacks',
    name: 'feedbacks',
    component: () => import(/* webpackChunkName: "retro" */ '../views/ReadFeedbacks.vue'),
    meta: {
      title: 'Les feedbacks du sprint',
      metaTags: [
        {
          name: 'description',
          content: 'Consulter les feedbacks de tous les collaborateurs du projet, de manière anonyme.',
        },
      ],
    },
  },
  {
    path: '/write-feedback',
    name: 'write-feedback',
    component: () => import(/* webpackChunkName: "retro" */ '../views/AddFeedback.vue'),
    meta: {
      title: 'Saisir un feedback pour le sprint en cours',
      metaTags: [
        {
          name: 'description',
          content: 'Saisir un feedback pour le sprint en cours et remonter ainsi les points positifs et négatifs !',
        },
      ],
    },
  },
  {
    path: '/show-participants',
    name: 'show-participants',
    component: () => import(/* webpackChunkName: "retro" */ '../views/ShowParticipants.vue'),
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
})

router.beforeEach((to, from, next) => {
  // This goes through the matched routes from last to first, finding the closest route with a title.
  // e.g., if we have `/some/deep/nested/route` and `/some`, `/deep`, and `/nested` have titles,
  // `/nested`'s will be chosen.
  const nearestWithTitle = to.matched.slice().reverse().find(r => r.meta && r.meta.title)

  // Find the nearest route element with meta tags.
  const nearestWithMeta = to.matched.slice().reverse().find(r => r.meta && r.meta.metaTags)

  const previousNearestWithMeta = from.matched.slice().reverse().find(r => r.meta && r.meta.metaTags)

  // If a route with a title was found, set the document (page) title to that value.
  if (nearestWithTitle) {
    (document as any).title = nearestWithTitle.meta.title
  }
  else if (previousNearestWithMeta) {
    (document as any).title = previousNearestWithMeta.meta.title
  }

  // Remove any stale meta tags from the document using the key attribute we set below.
  Array.from(document.querySelectorAll('[data-vue-router-controlled]')).map(el => el!.parentNode!.removeChild(el))

  // Skip rendering meta tags if there are none.
  if (!nearestWithMeta) return next();

  // Turn the meta tag definitions into actual elements in the head.
  (nearestWithMeta as any).meta.metaTags.map((tagDef: any) => {
    const tag = document.createElement('meta')

    Object.keys(tagDef).forEach(key => {
      tag.setAttribute(key, tagDef[key])
    })

    // We use this to track which meta tags we create so we don't interfere with other ones.
    tag.setAttribute('data-vue-router-controlled', '')

    return tag
  })
  // Add the meta tags to the document head.
    .forEach((tag: any) => document.head.appendChild(tag))

  next()
})

export default router
